//Safin Haque #2132492
public class Board{
	private Square[][] tictactoeBoard;
	
	
	
	public Board(){
	
		tictactoeBoard=new Square[3][3];
		
			for(int i=0;i<tictactoeBoard.length;i++){
				for(int j=0; j<tictactoeBoard[i].length;j++){
					tictactoeBoard[i][j]=Square.BLANK;
			}
		}
	}
	public boolean placeToken(int row, int col, Square playerToken){
		
		boolean token=false;
		
		if( ((tictactoeBoard.length-1)>=row && 0<=row) && ((tictactoeBoard.length-1)>=col && 0<=col))
		{
			token=true;
		}
		else{
			return false;
		}

		if(tictactoeBoard[row][col]==Square.BLANK)
		{
			tictactoeBoard[row][col]=playerToken;
			token=true;
		}
		
		return token;
		
	}
	
	public boolean checkIfFull(){
		for(int i=0;i<this.tictactoeBoard.length;i++){
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
				if(tictactoeBoard[i][j]==Square.BLANK)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		
		
		for(int i=0;i<this.tictactoeBoard.length;i++){
			int count=0;
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
				if(this.tictactoeBoard[i][j]==playerToken){
					count++;
				}
			}
			if(count==3){
				return true;
			}
		}
		return false;
		
	}
	 private boolean checkIfWinningVertical(Square playerToken){
		
		
		for(int i=0;i<this.tictactoeBoard.length;i++){
			 int count=0;
			for(int j=0;j<this.tictactoeBoard[i].length;j++){
				if(this.tictactoeBoard[j][i]==playerToken){
					count++;
				}
			}
			if(count==3){
				return true;
			}
		}
		return false;
		 
	 }
	 
	 public boolean  checkIfWinning(Square playerToken){
		boolean win=false;
		
		if(checkIfWinningVertical(playerToken)==true || checkIfWinningDiagonal(playerToken)==true || checkIfWinningHorizontal(playerToken)==true){
			win=true;
		}
		return win;
	 }
	 
	 private boolean checkIfWinningDiagonal(Square playerToken){
		 
		 for(int i=0;i<this.tictactoeBoard.length;i++){
			int count1=0;
			int temp=2;
			int count2=0;
		 
				if(this.tictactoeBoard[i][i]==playerToken){
					count1++;
				}
				
				if(this.tictactoeBoard[i][temp]==playerToken)
				{
					count2++;
					temp--;
				}
				
				if(count1==3 || count2==3){
				return true;
				}
			}
			
		return false;
	 }
	
	public String toString(){
		String table="";
		for(int i=0;i<tictactoeBoard.length;i++){
			for(int j=0; j<tictactoeBoard[i].length;j++){
				table+=tictactoeBoard[i][j] + " ";
			}
			table+="\n";
		}
		return table;
	}
}


 // || checkIfWinningDiagonal(playerToken)==true || checkIfWinningHorizontal(playerToken)==true