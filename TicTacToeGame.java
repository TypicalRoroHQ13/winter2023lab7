//Safin Haque #2132492
import java.util.Scanner;

public class TicTacToeGame{
	public static void main(String[]args){
		
		Scanner reader=new Scanner(System.in);
		System.out.println("Hello, welcome to my Tic-Tac-toe Game!");
		Board board=new Board();
		
		boolean gameOver=false;
		int player=1;
		Square playerToken=Square.X;
		
		
		while(gameOver==false){
			System.out.println(board);
			
			if(player==1)
			{
				playerToken=Square.X;
			}
			else
			{
				playerToken=Square.O;
			}
			
			System.out.println("Enter column number");
			int col=reader.nextInt();
			
			System.out.println("Enter row number");
			int row=reader.nextInt();
			
			while(board.placeToken(row,col,playerToken)==false){
				System.out.println("Location is either invalid or Taken, please re-input your columns and rows");
				
				col=reader.nextInt();
				
				row=reader.nextInt();
			}
			
			if(board.checkIfWinning(playerToken)==true){
				System.out.println("Player " + player + " is the winner");
				gameOver=true;
			}
			else
			if(board.checkIfFull()==true){
				System.out.println("It's a tie!");
				gameOver=true;
			}
			else
				player++;
				if(player>2)
				{
					player=1;
				}
		}
	System.out.println(board);


	}
}